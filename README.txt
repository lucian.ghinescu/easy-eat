In cadrul lucrării de diploma am realizat un dispozitiv ce ameliorează tremorul prezent la persoanele afectate de Parkinson
Pe post de actuatori au fost utilizate motoare BLDC.
Au fost utilizate două bibliboteci : 
MPU6050_light : https://github.com/rfetick/MPU6050_light
SimpleFOC :  https://github.com/simplefoc/Arduino-FOC
Lucrul cu acestea se poate realiza în două moduri: ESC și FOC.
In folderul Arduino:
 - subfolderul L298N este propusă realizarea dispozitivului utilizând acest driver
 - subfolderul MPU6050 propune un exemplu de obținere al datelor accelerometrice și giroscopice
 - subfolderul SimpleFOC contine algorimul de control pentru unghiul motoarelor și algoritmul de control final al dispozitivului

